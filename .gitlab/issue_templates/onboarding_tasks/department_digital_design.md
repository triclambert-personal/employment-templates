#### Marketing Brand and Design

<details>
<summary>New Team Member</summary>

1. [ ] Join the following slack channels:
    1. #marketing - This is the general marketing channel. Don't know where to ask a question? Start here.
    1. #marketing-design - All things marketing design are discussed here. Have questions? Need feedback? Stuck on an idea?
    1. #marketing-brand-and-digital
    1. #growth-marketing
1. [ ] Read the Marketing Handbook [Page](https://about.gitlab.com/handbook/marketing/)
1. [ ] Review the Marketing [OKRs](https://about.gitlab.com/handbook/marketing/#okrs).
1. [ ] Work with your manager to clone the [Corporate Marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing) to your computer (this is where our design source files are stored), this [Basic Git Tutorial](https://docs.google.com/document/d/1u7iNFnbD4Nj4aeLRFpmnzHewgKBjCwdyjvTuOND0rfA/edit#heading=h.na5uqd97wra) will help you get started (Brand team specific).

</details>


<details>
<summary>Manager</summary>

1. [ ] Provide [SketchApp](http://www.sketchapp.com/) license by adding a seat to the group license. This is administered by the UX department.
1. [ ] Provide [Adobe Creative Cloud](https://www.adobe.com/creativecloud.html) license by adding a seat to the group license. This is administered by `@luke` and `@mpreuss22`.
1. [ ] Provide [Mural](https://mural.co/) license by adding a seat to the group license. This is administered by `@clenneville`.
1. [ ] Provide Maintainer access to the [Corporate Marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing) by submitting an access request (Brand team specific).
</details>
