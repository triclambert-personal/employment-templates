#### For Application Security Only

<details>
<summary>New Team Member</summary>

1. [ ] Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
1. [ ] Request a light agent ZenDesk account: [support handbook](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff)
1. [ ] Once you have access to Slack, make sure to join the following security related channels (ordered a-z, not by importance):
    -  [ ] `#hackerone-feed`: HackerOne bug bounty program activity
    -  [ ] `#infrastructure-lounge`: All things infrastructure related
    -  [ ] `#ruby-security-mailing-lists`: Ruby security mailing list updates (optional)
    -  [ ] `#sec-appsec`: Application Security team channel
    -  [ ] `#sec-appsec-standup`: Application Security standup updates
    -  [ ] `#security`: General security related discussions
    -  [ ] `#security-culture`: Security culture team channel
    -  [ ] `#security-department`: Security department channel
    -  [ ] `#security-infrasec`: Infrastructure Security team channel
    -  [ ] `#security-research`: Security Research team channel
    -  [ ] `#security-team-standup`: Security department standup updates
1. [ ] Familiarize yourself with the [labels](https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues) we use for communicating severity and priority of security-related issues.
   1. [ ] We use CVSS v3 for calculating the severity of security issues. Information about CVSS scoring system is [here](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/cvss-calculation.html).
   1. [ ] Check out and bookmark our custom [CVSS score and bounty calculator](https://gitlab-com.gitlab.io/gl-security/appsec/cvss-calculator/)
1. [ ] Familiarize yourself with the [HackerOne process](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/hackerone-process.html)
1. [ ] Familiarize yourself with the other [application security runbooks](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/)
1. [ ] Follow [these steps to get a GitLab Ultimate licence](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee) which you can use on GitLab Enterprise Edition locally
1. [ ] Check out the [HackerOne program statistics](https://hackerone.com/gitlab/program_statistics) and browse some submissions from our Top Earners
1. [ ] Explore the different [engineering groups](https://about.gitlab.com/handbook/product/categories/) at GitLab and note the ones you find interesting. This will be helpful for the [Stable Counterpart](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/stable-counterparts.html) system later.
1. [ ] Check out the AppSec Weekly Triage Rotation spreadsheet (find a link to it in the `#sec-appsec` channel subject) and arrange to shadow team members on their next rotations:
    - [ ] [HackerOne](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/hackerone-process.html)
    - [ ] [Triage Rotation (mentions and issues)](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/triage-rotation.html)
    - [ ] [Security Release](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/security-engineer.md)
1. [ ] Explore the [security department projects](https://gitlab.com/gitlab-com/gl-security) on GitLab.
1. [ ] Review [some past AppSec reviews](https://gitlab.com/gitlab-com/gl-security/appsec/appsec-reviews/-/issues?scope=all&state=closed) and note how AppSec interacts with other team members, writes up findings, progresses them through to completion, etc
</details>
