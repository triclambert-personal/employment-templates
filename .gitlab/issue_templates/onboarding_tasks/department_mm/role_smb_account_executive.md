#### SMB Account Executive
<details>
<summary>New Team Member</summary>

1. [ ] Meet with your buddy and ASM on day 1 for your onboarding. Create a google document to track onboarding related notes and questions outside of this issue.  This can turn into a career development shared document or starting 1:1.  Document lessons learned with obstacles faced in onboarding.
1. [ ] Read the Commercial Sales handbook. Visit [Getting Started](https://about.gitlab.com/handbook/sales/commercial/#getting-started-as-a-new-hire)
1. [ ] [Ensure you read through and fully understand the Required 7](https://about.gitlab.com/handbook/sales/commercial/enablement/required7/). 
1. [ ] Review [previous training decks](https://about.gitlab.com/handbook/sales/commercial/smb/#previous-training-sessions) on the handbook
1. [ ] Review previous training sessions [on the Commercial Sales Training playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp62aFrHOjcJnqS6GkFJyhL) in GitLab Unfiltered
1. [ ] Start exploring ideas with peers and ASM for your [Give Back Project](https://about.gitlab.com/handbook/sales/commercial/#give-back-project). Create a New Issue using the template
1. [ ] Review [the SMB AE handbook page](https://about.gitlab.com/handbook/sales/commercial/smb/), in particular;
   1. [ ] [Join relevant Slack channels](https://about.gitlab.com/handbook/sales/commercial/smb/#reaching-the-smb-team-internally)
   1. [ ] [Call Preps](https://about.gitlab.com/handbook/sales/commercial/smb/#call-preps)
   1. [ ] [The tools used](https://about.gitlab.com/handbook/sales/commercial/smb/#tools)
   1. [ ] [How you get paid](https://about.gitlab.com/handbook/sales/commercial/smb/#compensation)
   1. [ ] [Commercial SA Engagement Model](https://about.gitlab.com/handbook/customer-success/solutions-architects/processes/commercial/)
1. [ ] Listen to at least five recent Commercial calls on Chorus in the first week. Take notes in your document by listing the account, how it was sourced, goal of the call and change in close plan, list any questions/objections faced, main learnings. 
1. [ ] Set up 1:1s and coffee chats with team members.  Start with your immediate team and broaden out to all regions. 
1. [ ] Join call preps with Peer team members and offer input where applicable in preparation and feedback. Take notes in SFDC. Link to notes where needed in doc. 
1. [ ] Join calls to shadow Peer team members and offer input in feedback. Introduce yourself, take notes in SFDC, and contribute as needed. Link to notes where needed in doc.
1. [ ] Set up a session with a peer team member to do a full opportunity review and go through R7 daily process including Command Plans.  If available in the same time, walk through the quote creation process. 
   1. [ ] Review [the Sales Order Processing page in the handbook](https://about.gitlab.com/handbook/sales/field-operations/order-processing/) which outlines how we use SalesForce and how to quote. 
   1. [ ] Review the [Alliances Handbook](https://about.gitlab.com/handbook/alliances/) and Private handbook linked to get an overview and how orders are processed when it includes an Alliances partner.
   1. [ ] Collaborate on quotes with each team member in your region - ask peer Account Executives when an active deal requires a quote. Assist with R7 on the opportunity and follow it to closing. This can be any size and type of opportunity.  Try to include New, Amendment, and Renewal quote types in this learning. 
1. [ ] Review [the Forecasting page on the handbook](https://about.gitlab.com/handbook/sales/forecasting/)
   1. [ ] [SMB Example Forecasting formula](https://about.gitlab.com/handbook/sales/commercial/smb/#forecasting-wip)
   1. [ ] Review [how to use Clari](https://about.gitlab.com/handbook/sales/forecasting/#clari-for-salespeople-instructional-videos)
1. [ ] Other important meetings to watch and read through:
   1. [ ] [Ingredients of a successful Commercial Account Executive](https://docs.google.com/presentation/d/1uPg38oDXwFT3Sr-UU3fT-v-9ky34fQRo-n4qAAOdCro/edit?usp=sharing)
   1. [ ] Look through the [Commercial Pitch Deck Hall of Fame](https://drive.google.com/drive/folders/1clOKleNK0O6wr2B0FT9ZXwgQoV90wHYs?usp=sharing) to understand the minimum quality standard expected in your pitch decks.
   1. [ ] [Pricing changes and EoA overview](https://www.youtube.com/watch?v=zG9d1y2jE14)
   1. [ ] [President’s Club criteria](https://about.gitlab.com/handbook/sales/club/#overview)
   1. [ ] [Worldwide Field Sales Call notes](https://docs.google.com/document/d/1YF4mtTAFmH1E7HqZASFTitGQUZubd12wsLDhzloEz3I/edit?usp=sharing) 

</details>

<details>
<summary>ASM</summary>

1. [ ] Assign Mobility/Onboarding buddy
1. [ ] Ensure new team member is in team slack channels if not already joined
1. [ ] When most of the tasks here are completed, start and link related territory transfer
1. [ ] Run through pipeline checklist document and ensure new team member has saved views, dashboards, and reports in SFDC
1. [ ] Run through R7 practice and approach to account and territory planning.  Share relevant R7 and team dashboards/reports 

</details>
