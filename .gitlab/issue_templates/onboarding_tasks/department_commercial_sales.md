#### Sales Division


<details>
<summary>Manager</summary>

1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing) in our Google Docs.
1. [ ] Manager: Create and submit an issue using [this issue template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new#) to inform Sales Ops of the territory that the new team member will assume, paired SDR (if any), and identified opportunity holdovers that should remain with the current owner (if any). Accounts will be reassigned to new hire and transition plan needs to be approved per account.
1. [ ] Manager: Slack Sales Strategy (@Swetha) the new team member's quota information for upload into Salesforce.

</details>


<details>
<summary>New Team Member</summary>

1. [ ] New team member: Check the Sales Onboarding page of the Handbook (https://about.gitlab.com/handbook/sales/onboarding/)
1. [ ] New team member: Complete the [Sales Quick Start (SQS) Pre-work in EdCast](https://gitlab.edcast.com/pathways/sales-quick-start). It is recommended to start the SQS EdCast pre-work by your 3rd or 4th day with the company. If you have questions, please reach out to John Blevins (@jblevins608).
1. [ ] New team member: You can check the Handbook on more information on the learning path and assignments https://about.gitlab.com/handbook/sales/onboarding/sales-learning-path/ 
1. [ ] New team member: Once you complete the SQS Pre-work and the CoM and MEDDPICC e-learning courses, attend the Sales Quick Start (SQS) In- Person Training (or Virtual Training due to COVID-19)
1. [ ] New team member: Follow [these instructions](https://help.datafox.com/hc/en-us/articles/227081328-User-Setup-Connect-your-DataFox-User-to-your-Salesforce-Account) to link your DataFox Account with your Salesforce Account.
1. [ ] New team member: For all roles EXCEPT PubSec Inside Sales Rep and SMB Customer Advocate, please consult with your manager to determine whether or not you need access to Zendesk Light Agent. If yes, follow [these instructions](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff) to request access. Note: SMB Customer Advocates are provisioned "GitLab Staff Role" access to Zendesk and PubSec Inside Sales Reps are provisioned "Light Agent" access to Zendesk-federal within their role-based access request template above. PubSec Strategic Account Leaders may rely on their PubSec Inside Sales Rep for Zendesk-federal related matters.

</details>


<details>
<summary>Sales Enablement</summary>

1. [ ] Sales Enablement (@jblevins608): Add new Sales Team Member to Sales Quick Start Learning Path in EdCast. New sales team member will receive an email prompting them to login to EdCast to begin working through the Sales Quick Start Learning Path. This Learning Path is designed to accelerate the new sales team member's time to productivity by focusing on the key sales-specific elements they need to know and be able to do within their first several weeks at GitLab.

</details>


<details>
<summary>Sales Strategy</summary>

1. [ ] Sales Strategy (@Swetha): Set BCR, SCR (if applicable), and prorated quota and deliver participant plan/schedule
1. [ ] Sales Strategy (@Swetha): Deliver Participant Schedule.

</details>


<details>
<summary>Marketing</summary>

1. [ ] Marketing (@bethpeterson): Two weeks after start date, go live with update to Territory Model in LeanData with new territory assignments.

</details>

