## Development Department

<details>
<summary>New Reporting Manager</summary>

1. [ ] Manager: Ensure that the `job title specialty` field in BambooHR accurately reflects the team member's new group/team. This is essential as this metric feedbacks into Sisense to report of Development data. This can be updated by submitting a [job information change](https://about.gitlab.com/handbook/people-group/promotions-transfers/#job-information-change-in-bamboohr) in BambooHR.

</details>
