## FOR ENGINEERING ONLY (Devs, PEs, SEs)

<summary>Manager</summary>

1. [ ] Manager: Remove former GitLab Team Member's' GitHub.com account from the [gitlabhq organization](https://github.com/orgs/gitlabhq/people) (if applicable).
1. [ ] For former Developers (those who had access to part of the infrastructure), and Production GitLab Team Members: copy offboarding process from [infrastructure](https://dev.gitlab.org/cookbooks/chef-repo/blob/master/doc/offboarding.md) for offboarding action.
1. [ ] Manager: Remove access to PagerDuty if applicable.
1. [ ] Manager (For Build Engineers): Remove team member as a member to the GitLab Dev Digital Ocean account https://cloud.digitalocean.com/settings/team.
1. [ ] Manager: Remove from GitLab Docker Hub teams if applicable.
1. [ ] Manager: Remove from any [on-call obligations](https://docs.google.com/spreadsheets/d/10uI2GzqSvITdxC5djBo3RN34p8zFfxNASVnFlSh8faU/edit#gid=1066364624), and notify coordinator
1. [ ] Manager: Replace mentions in handbook's [stages.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml) as this is the look-up reference for many processes. 

<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Remove any development VMs. Send a merge request to [the dev-resources repo](https://gitlab.com/gitlab-com/dev-resources) to remove `dev-resources/name-surname.tf`. Follow the instructions [here](https://gitlab.com/gitlab-com/dev-resources/tree/master/dev-resources#how-do-i-delete-an-instance-i-dont-need-anymore).

<summary>Product Licenses</summary>

- [ ] **JetBrains** (@leipert, @samdbeckham): Go to the [user management](https://account.jetbrains.com/login) and search for the team member, revoke their licenses.

<summary>Other</summary>
- [ ] @kristie.thomas @jennifergarcia20: Remove team member from Ally.io
