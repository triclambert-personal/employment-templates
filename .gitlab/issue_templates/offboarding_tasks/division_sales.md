## FOR SALES ONLY

<summary>Sales/Marketing Ops</summary>

1. [ ] Sales/Marketing Ops: Disable User and Other Actions
    * [ ] @Astahn @jenybae: Update Sale Operations Sponsored Reports/Dashbaords listed on [The Sales Operations Handbook Page](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/#sales-operations-sponsored-dashboards-and-maintenance)
    * [ ] @Astahn @jenybae: Salesforce Accounts and Opportunities: REASSIGN all accounts and open opportunities (do Not Reassign CLOSED WON/LOST OPPS!) based on direction provided by RD/VP within 24 hours of termination. Any named accounts will be transitioned either to RD or designated SAL/AM.
    * [ ] @Astahn @jenybae: Update Territory Model in LeanData with temporary territory assignments and Sales Territories in Handbook for SALs/AEs.      
    * [ ] @Astahn @jenybae: If offboarding Sales ASM/RD/VP, replace the manager field in SFDC with the updated manager
    * [ ] @Astahn @jenybae: Update the [Sales Territories Handbook page](https://about.gitlab.com/handbook/sales/territories/) with the updated manager or Rep.  If unknown, replace with TBD.
    * [ ] @bethpeterson Salesforce Leads and contacts: REASSIGN all leads and contacts based on direction provided by SDR leadership within 24 hours of termination.
    * [ ] @bethpeterson: Update Territory Model in LeanData with temporary territory assignments for SDRs.
    * [ ] Outreach - @gillmurphy: First, lock the user, Second, turn off both "Enable sending messages from this mailbox" and "Enable syncing messages from this mailbox".
    * [ ] ZoomInfo - @robrosu: Deactivate and remove former team member from ZoomInfo
    * [ ] LeanData - @bethpeterson: Remove from any lead routing rules and round robin groups.
    * [ ] LinkedIn Sales Navigator - @jburton Disable user, remove from team license.
1. [ ] Ringlead: @Astahn @jenybae Remove team member from Ringlead if applicable.
1. [ ] Terminus: @gillmurphy Remove team member.
1. [ ] IMPartner: @ecepulis @KJaeger Remove team member from IMPartner if applicable.
1. [ ] Xactly Incent: @lisapuzar @Swethakashyap @pravi1 @sophiehamann
1. [ ] O'Reilly: @jfullam @tbopape Remove team member from O'Reilly if applicable.
