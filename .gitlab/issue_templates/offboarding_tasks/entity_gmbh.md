## For GitLab GmbH employees only

<summary>People Experience</summary>

1. [ ] People Experience: Inform payroll (@sszepietowska  @nprecilla)  of last day of employment.
1. [ ] People Experience: Insure a resignation letter with a wet signature has been saved in BambooHR > Termination folder. 
