Ta-New-Ki call email 

Subject: Join us for a GitLab welcome call 


Hi {NAME},
As we count down to your first day at GitLab, we’d like to invite you to a pre-onboarding “Ta-New-Ki” welcome call!
This is a great opportunity to meet other new team members, get helpful tips for onboarding, and ask our team any lingering questions ahead of your start date. At GitLab, we call this type of Q&A session an “Ask Me Anything” (AMA), so any and all questions are welcomed. 
The call is completely optional, but we’d love to see you there! 

Details: 
Zoom video call Link | {Link}
Planning to join us? Sign up here (https://forms.gle/VRGnJbSHgQAzFxNJ6).
Please let us know if you have questions for now.

All the best,
{YOUR NAME}
{YOUR TITLE} 
