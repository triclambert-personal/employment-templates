Hi [New Team Member Name],

Our 2019 Contribute event in New Orleans, LA, USA (May 8-14, 2019) is fast approaching. 

You are receiving this message because you are scheduled to start work in the weeks and days before the event. The deadline to register is May 5, 2019 but you are only able to register with a GitLab email address. Because GitLab can only grant access to our all systems on Day 1, we wanted to give you more information on getting ready for Contribute, should you decide to attend. There are many things you can do to get ready so that the final step will be registration on your first day.

First, please read our (GitLab Contribute) [https://about.gitlab.com/company/culture/contribute/] handbook page. Also look at the (GitLab New-Orleans-2019) [https://gitlab.com/gitlabcontribute/new-orleans] project page, documents and issues. The (FAQ issue) [https://gitlab.com/gitlabcontribute/new-orleans/issues/20] is a great resource. The most recent informational page that should answer most of your questions is this (FAQ) [https://gitlab.com/gitlabcontribute/new-orleans/blob/master/faqs.md#i-have-a-new-team-member-starting-close-to-contribute-can-they-still-come] page. We will also add you as a Single Channel Guest to the contribute2019 Slack channel.

Another wonderful tool is our GitLab Contribute app. Please download the CrowdCompass AttendeeHub (Apple) [https://itunes.apple.com/us/app/crowdcompass-attendeehub/id604224729?mt=8] (Android) [https://play.google.com/store/apps/details?id=com.crowdcompass.app4815162342&hl=en_US] and search for [GitLab code]. Our event will appear and you can view the Event Guide and Schedule without logging in. Think about which events and workshops are most exciting to you so that when you register you can know what to choose.

If you are able to attend and live in the US, feel free to purchase flights in advance. You will be able to submit them for reimbursement on your first days once you set up your Expensify account. Please be sure to research if you need a visa to attend this event. A good place to start is (this issue) [https://gitlab.com/gitlabcontribute/new-orleans/issues/10]. If you live in the US or any other part of the world and need assistance purchasing flights, we can purchase your flights for you. Please send confirmation that all your necessary travel documents are in hand and up to date, your Full Name (as it appears on your Government Tavel ID document), Date of Birth, Known Traveler Number, and full flight details. Do not worry about lodging, as GitLab has reserved multiple rooms for new GitLabbers just like you.

Feel free to reach out to People Ops (cc'd) if you have any questions or need help!

Thank you!